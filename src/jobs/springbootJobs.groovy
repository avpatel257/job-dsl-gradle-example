String basePath = 'spring-boot-docker'
String repo = 'https://avpatel257@bitbucket.org/avpatel257/spring-boot-docker'

job("$basePath" + "-build") {
    scm {
        github repo
    }
    triggers {
        scm 'H/5 * * * *'
    }
    steps {
        maven('clean package')
    }
}